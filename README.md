# Rock Paper Scissors
Rock paper scissors implementation in JavaScript for The Odin Project's [Web Development 101](https://www.theodinproject.com/courses/web-development-101) course.

Initial implementation only runs in console, front-end to be added later.

## How to Play
### Console Version
- Open `index.html` in your browser and open Developer Tools (`F12` in Chrome).
- Type `game()` in the console to start the game.

### Web Version
- Open `index.html` in your browser and make a selection from either **Rock**, **Paper**, or **Scissors**.
- The result of the game will be output below the buttons, and it will track how many games you have won.
- A winner will be announced once you or the computer have 5 wins.