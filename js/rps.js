const options = ["rock", "paper", "scissors"]

String.prototype.format = function () {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
}

function computerPlay() {
    return options[Math.floor(Math.random() * 3)]
}

function capitalize(input) {
    input = input.toLowerCase()
    input = input.charAt(0).toUpperCase() + input.slice(1)
    return input
}

function pickWinner(player, computer) {
    if (player == 0) {
        switch (computer) {
            case 1:
                return computer

            default:
                return player
        }
    } else if (player == 1) {
        switch (computer) {
            case 0:
                return player
            default:
                return computer
        }
    } else {
        switch (computer) {
            case 0:
                return computer
            default:
                return player
        }
    }
}

function playRound(playerChoice) {
    const playerVal = options.indexOf(playerChoice.toLowerCase())
    const computerChoice = computerPlay()
    const computerVal = options.indexOf(computerChoice.toLowerCase())
    let result = "Lose"
    let winner;
    let loser;

    if (playerVal == computerVal) {
        result = "Tie"
    } else {
        winVal = pickWinner(playerVal, computerVal)
        if (winVal == playerVal) {
            loser = computerChoice
            result = "Win"
        } else {
            loser = playerChoice
        }
        winner = options[winVal]
    }

    const message = "You {0}!".format(result)
    if (result == "Tie") {
        return message
    } else {
        return "{0} {1} beats {2}".format(message, capitalize(winner), capitalize(loser))
    }
}

function getResult(message) {
    var words = message.split("!")
    return words[0].slice(4)
}

function game() {
    const numRounds = 5;
    var wins = 0;
    for (let i = 0; i < numRounds; i++) {
        console.log("Starting Round {0}".format(i + 1))
        let playerChoice = prompt("Player - Make your choice: (Rock/Paper/Scissors)")
        let computerChoice = computerPlay()
        message = playRound(playerChoice)
        result = getResult(message)
        switch (result) {
            case "Win":
                wins++;
                break;
            case "Tie":
                i--;
                console.log("Tied, replay round!")
                break;
            default:
                break;
        }
        console.log(message)
    }
    console.log("You won {0} out of {1} games!".format(wins, numRounds))
}

const container = document.querySelector('#container');
const buttons = document.querySelectorAll('button');
var playerWins = 0;
var computerWins = 0;

function showResult(playerChoice) {
    const resultText = playRound(playerChoice);
    var resultPar = document.querySelector('#resultText');
    if (resultPar) {
        resultPar.textContent = resultText;
    } else {
        resultDiv = document.createElement('div');
        resultDiv.classList.add('result');
        resultPar = document.createElement('p');
        resultPar.setAttribute('id', 'resultText');
        resultPar.textContent = resultText;

        resultDiv.appendChild(resultPar);
        container.appendChild(resultDiv);
    }
    return resultText;
}

function checkWins(result) {
    if (getResult(result) == "Win") {
        playerWins++;
    } else if (getResult(result) == "Lose") {
        computerWins++;
    }
    if (playerWins == 5 || computerWins == 5) {
        let winner;
        if (playerWins == 5) {
            winner = "You are ";
        } else {
            winner = "Computer is ";
        }
        buttons.forEach((button) => {
            button.disabled = true;
        })
        var winElement = document.querySelector('.wins');
        winElement.textContent = winner + "the winner!";
        return true;
    }
    return false;
}

function updateWins(resultText) {
    if (getResult(resultText) == "Win") {
        // playerWins++;
        var winElement = document.querySelector('.wins');
        if (winElement) {
            winElement.textContent = "Wins: " + playerWins;
        } else {
            winDiv = document.createElement('div');
            winDiv.classList.add('wins');
            winElement = document.createElement('p');
            winElement.setAttribute('id', 'winText');
            winElement.textContent = "Wins: " + playerWins;

            winDiv.appendChild(winElement);
            container.appendChild(winDiv);
        }
    }
}

buttons.forEach((button) => {
    button.addEventListener('click', (e) => {
        let choice = e.target.firstChild.nodeValue;
        let result = showResult(choice);
        let winStatus = checkWins(result);
        if (!winStatus) {
            updateWins(result);
        }
    });
});
